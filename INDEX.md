# I16ENV

Small package so you can compile programs using IA-16 GCC without installing the full DJGPP environment.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## I16ENV.LSM

<table>
<tr><td>title</td><td>I16ENV</td></tr>
<tr><td>version</td><td>2024-05-26</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-05-27</td></tr>
<tr><td>description</td><td>Environment for IA-16 GCC without DJGPP</td></tr>
<tr><td>summary</td><td>Small package so you can compile programs using IA-16 GCC without installing the full DJGPP environment.</td></tr>
<tr><td>keywords</td><td>boot cd dvd language message autoexec config system driver extension</td></tr>
<tr><td>author</td><td>James Hall</td></tr>
<tr><td>maintained&nbsp;by</td><td>James Hall</td></tr>
<tr><td>primary&nbsp;site</td><td>https://ibiblio.org/pub/micro/pc-stuff/freedos/files/devel/c/gcc-ia16/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Public Domain](LICENSE)</td></tr>
</table>
